
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });


window.flashSlideUp = function(flash) {
    flash.slideUp(500);
};

window.flashMessage = function (message, flash_class) {
    var flash = $('#flash-message');
    if (flash.length === 0) {
        flash = $('<div/>')
            .attr('id', 'flash-message')
            .attr('role', 'alert')
            .addClass('alert text-center ' + 'alert-' + flash_class);
        $('#app').prepend(flash);
    }

    flash.text(message);
    flash.slideDown(500);
    window.flashTimeout = setTimeout(function() { flashSlideUp(flash); }, 3000);
    flash.on('click', function() { flashSlideUp(flash); });
};

(function($) {
    var flash = $('#flash-message');
    flash.slideDown(500);
    window.flashTimeout = setTimeout(function() { flashSlideUp(flash); }, 3000);
    flash.on('click', function() { flashSlideUp(flash); });
})(jQuery);