<?php
$totalValue = 0.00;
$pos = 0;
?>

@extends('layouts.app')

@section('content')
    <h2 class="page-header text-center">Carrinho</h2>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row margin-bottom-40">
                <div class="col-md-6">
                    <h4 class="">
                        Produtos
                    </h4>

                    @foreach($products as $product)
                        <div class="row product">
                            <div class="col-md-12">
                                <div class="media">
                                    <div class="media-left">
                                        <img class="" src="{{ asset($product->file) }}" alt="..."
                                             style="width: 120px; height: 180px;">
                                    </div>

                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="{{ $product->path() }}">{{ $product->name }}</a>
                                        </h4>

                                        <div class="text-justify">
                                            {{ str_limit($product->description, 200, '...') }}
                                        </div>

                                        <div style="position: absolute; bottom: 15px; left: 150px;">
                                            <a href="" class="delete-product" data-id="{{ $product->id }}"
                                               data-pos="{{ $pos }}">Remover</a>
                                        </div>

                                        <div class="value" style="position: absolute; bottom: 15px; right: 15px;">
                                            {!! money($product->price) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="line"></div>
                            </div>
                        </div>

                        <?php
                        $totalValue += $product->price;
                        $pos++;
                        ?>
                    @endforeach

                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                Valor Total: &nbsp
                                <div id="total_value">{!! money($totalValue) !!}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-5 col-md-offset-1">
                    <h4 class="">
                        Endereço
                    </h4>
                    <form action="/orders/store" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" id="create-order" class="hidden"/>
                        @foreach($addresses as $address)
                            <div class="address">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="address" value="{{ $address->id }}" required>
                                        @include('partials.addresses.show')
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="/products" class="btn btn-info">
                        Voltar as compras
                    </a>

                    <form action="/carts" method="POST" style="display: inline;">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type='submit' class="btn btn-danger" value="">Limpar carrinho</button>
                    </form>

                    <div class="pull-right">
                        <label for="create-order" tabindex="0" class="btn btn-success">
                            Finalizar compra
                        </label>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(function () {
            $('.delete-product').on('click', function (e) {
                e.preventDefault();

                let $this = $(this);
                let product_id = $this.data('id');
                let position = $this.data('pos');

                $.ajax({
                    dataType: "json",
                    url: '{{ url('/carts/product') }}',
                    data: {'product_id': product_id, 'position': position},
                    type: 'DELETE',
                    success: function (result) {
                        if (result.result) {
                            $this.closest('.product').remove();
                            $('#total_value').html(result.value);
                            flashMessage(result.message, 'success');
                        } else {
                            flashMessage(result.message, 'danger')
                        }
                    }
                })
            })
        })
    </script>
@endsection