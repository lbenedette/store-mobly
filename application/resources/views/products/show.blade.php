@extends('layouts.app')

@section('content')
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="text-center">
                    <img class="" src="{{ asset($product->file) }}" alt="..."
                         style="width: 220px; height: 300px;">
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    {{ $product->name }}
                </div>
            </div>

            <div class="panel-body">
                <p class="text-justify">{{ $product->description }}</p>

                <hr>

                Características do produto:
                <ul>
                    @foreach($product->characteristics() as $characteristic)
                        <li>{{ $characteristic }}</li>
                    @endforeach
                </ul>

                <hr>

                @foreach($product->categories() as $category)
                    <a href="">
                        <span class="label label-default">{{ $category }}</span>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="pull-right margin-top-5"><span class="price">{!! money($product->price) !!}</span></div>

                <div class="pull-left">
                    <a class="btn btn-success"
                       onclick="event.preventDefault(); document.getElementById('buy-product').submit();">
                        Comprar
                    </a>
                </div>

                <form id="buy-product" action="{{ $product->path() }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection
