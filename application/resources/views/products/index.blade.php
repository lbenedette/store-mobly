@extends('layouts.app')

@section('content')
    <h2 class="page-header text-center">Produtos</h2>

    <div class="row">
        @foreach($products as $product)

            @include('partials.products.index')

        @endforeach
    </div>
@endsection
