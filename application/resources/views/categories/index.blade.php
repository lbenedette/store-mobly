@extends('layouts.app')

@section('content')
    <h2 class="page-header text-center">Categorias</h2>

    <div class="row">
        @foreach($categories as $category)
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        {{ $category }}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
