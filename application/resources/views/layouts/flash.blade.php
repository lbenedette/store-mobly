@if (Session::has('flash_message'))
    <div id="flash-message" class="alert text-center alert-{{ session('flash_class') }}" role="alert" style="display: none;">
        {{ session('flash_message') }}
    </div>
@endif
