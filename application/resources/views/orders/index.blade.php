@extends('layouts.app')

@section('content')
    <h2 class="page-header text-center">Pedidos</h2>

    <div class="row">
        @foreach($orders as $order)

            @include('partials.orders.order')

        @endforeach
    </div>
@endsection
