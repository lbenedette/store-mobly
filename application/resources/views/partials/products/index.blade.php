<div class="col-md-4">
    <div class="thumbnail" style="height: 520px;">
        <div class="text-center">
            <img class="margin-top-20" src="{{ asset($product->file) }}" alt="..."
                 style="width: 220px; height: 300px;">
        </div>

        <div class="caption">
            <h4><a href="{{ $product->path()  }}">{{ $product->name }}</a></h4>
            <p class="text-justify">
                {{ str_limit($product->description, 50, '...') }} <br>

                {!! money($product->price) !!}
            </p>

            <div class="margin-top-5 product-category">
                @foreach($product->categories() as $category)
                    <span class="label label-info">{{ $category }}</span>
                @endforeach
            </div>

        </div>
    </div>
</div>