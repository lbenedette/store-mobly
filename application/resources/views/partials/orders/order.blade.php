<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-body">
            Pedido realizado em: {{ $order->created_at }}

            <div class="pull-right">
                Número do pedido: {{ $order->id }}
            </div>

            <br>
            Total: {!! money($order->value) !!}

            <div class="line"></div>

            @foreach($order->products as $product)
                <div class="media">
                    <div class="media-left">
                        <img class="" src="{{ asset($product->file) }}" alt="..."
                             style="width: 90px; height: 120px;">
                    </div>

                    <div class="media-body">
                        <h4 class="media-heading"><a href="{{ $product->path() }}">{{ $product->name }}</a></h4>

                        <div class="text-justify">
                            {!! money($product->price) !!}
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="line"></div>

            @include('partials.addresses.show', ['address' => $order->address()])
        </div>
    </div>
</div>