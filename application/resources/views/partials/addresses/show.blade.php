<ul class="list-unstyled">
    <li><strong>{{ $address->name }}</strong></li>
    <li>{{ $address->address }}</li>
    <li>{{ $address->complement }}</li>
    <li>{{ $address->city }} - {{ $address->state }}, {{ $address->post_code }}</li>
    <li>{{ $address->country }}</li>
    <li>Número de telefone: {{ $address->phone }}</li>
</ul>