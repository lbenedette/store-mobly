@extends('layouts.app')

@section('content')
    <h2 class="page-header text-center">Novo endereço</h2>

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="/addresses/create">
                    {{ csrf_field() }}

                    @include('addresses.form')
                </form>
            </div>
        </div>
    </div>

@endsection