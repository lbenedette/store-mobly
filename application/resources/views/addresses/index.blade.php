@extends('layouts.app')

@section('content')
    <h2 class="page-header text-center">Endereços</h2>

    <div class="row">
        <div class="col-md-4">
            <a href="/addresses/create">
                <div class="address address-new">
                    <h3 style="line-height: 160px;">Adicionar endereço</h3>
                </div>
            </a>
        </div>

        @foreach($addresses as $address)

            @include('partials.addresses.address')

        @endforeach

    </div>
@endsection
