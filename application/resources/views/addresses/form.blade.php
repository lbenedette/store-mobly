<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">Nome Completo</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
               required autofocus>

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    <label for="address" class="col-md-4 control-label">Endereço</label>

    <div class="col-md-6">
        <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}"
               required autofocus>

        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('complement') ? ' has-error' : '' }}">
    <label for="complement" class="col-md-4 control-label">Complemento</label>

    <div class="col-md-6">
        <input id="complement" type="text" class="form-control" name="complement" value="{{ old('complement') }}"
               autofocus>

        @if ($errors->has('complement'))
            <span class="help-block">
                <strong>{{ $errors->first('complement') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
    <label for="city" class="col-md-4 control-label">Cidade</label>

    <div class="col-md-6">
        <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}"
               required autofocus>

        @if ($errors->has('city'))
            <span class="help-block">
                <strong>{{ $errors->first('city') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
    <label for="state" class="col-md-4 control-label">Estado</label>

    <div class="col-md-6">
        <input id="state" type="text" class="form-control" name="state" value="{{ old('state') }}"
               required autofocus>

        @if ($errors->has('state'))
            <span class="help-block">
                <strong>{{ $errors->first('state') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
    <label for="country" class="col-md-4 control-label">País</label>

    <div class="col-md-6">
        <input id="country" type="text" class="form-control" name="country" value="{{ old('country') }}"
               required autofocus>

        @if ($errors->has('country'))
            <span class="help-block">
                <strong>{{ $errors->first('country') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('post_code') ? ' has-error' : '' }}">
    <label for="post_code" class="col-md-4 control-label">CEP</label>

    <div class="col-md-6">
        <input id="post_code" type="text" class="form-control" name="post_code" value="{{ old('post_code') }}"
               required autofocus>

        @if ($errors->has('post_code'))
            <span class="help-block">
                <strong>{{ $errors->first('post_code') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    <label for="phone" class="col-md-4 control-label">Telefone</label>

    <div class="col-md-6">
        <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}"
               autofocus>

        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            Salvar
        </button>
    </div>
</div>