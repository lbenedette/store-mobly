<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            // Salvar o endereço no formato json, assim mesmo alterando
            // ou deletando o endereço é possível recuperar as informações
            $table->json('address');
            $table->decimal('value')->default('0.0');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });

        Schema::create('order_product', function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            // O mesmo feito com endereços poderia ter sido feita com produtos,
            // porém como o sistema ainda é simples e não existe descontos, ou
            // edição de produto, vamos manter o id do produto.
            $table->unsignedInteger('product_id');
            $table->primary(['order_id', 'product_id']);

            $table->foreign('order_id')
                ->references('id')
                ->on('orders');

            $table->foreign('product_id')
                ->references('id')
                ->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product');
        Schema::dropIfExists('orders');
    }
}
