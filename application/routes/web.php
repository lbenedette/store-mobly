<?php

use Illuminate\Support\Facades\Redis;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/products');
});

Auth::routes();

Route::get('/products', 'ProductsController@index');
Route::get('/products/{product}', 'ProductsController@show');
Route::post('/products/{product}', 'ProductsController@buy');

Route::get('/addresses', 'AddressesController@index');
Route::get('/addresses/create', 'AddressesController@create');
Route::post('/addresses/create', 'AddressesController@store');

Route::get('/carts', 'CartsController@index');
Route::delete('/carts', 'CartsController@destroy');
Route::delete('/carts/product', 'CartsController@removeProduct');

Route::get('/orders', 'OrdersController@index');
Route::post('/orders/store', 'OrdersController@store');

Route::get('/categories', 'CategoriesController@index');
