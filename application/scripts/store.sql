INSERT INTO store.users (id, name, email, password, remember_token, created_at, updated_at) VALUES (1, 'John Doe', 'john@example.com', '$2y$10$LF0VKjRKDMTilIIIAeIBbuDxVgKBEk7YkRDYoCOC4jUzzkck94KdK', null, '2018-05-27 05:35:26', '2018-05-27 05:35:26');
INSERT INTO store.users (id, name, email, password, remember_token, created_at, updated_at) VALUES (2, 'Leonardo Lopes', 'blopesleo@outlook.com', '$2y$10$oDPJ42PGd6RVUQUvRlkdDuIJGGztgTm4xLuMC17c6u5oNGL8ytAti', 'VgypyitRdmN72UTpu4io6h6hy7Bh5tzACwsUNQe1WLinVr5frt0pWPtzrsxg', '2018-05-27 17:18:52', '2018-05-27 17:18:52');

INSERT INTO store.products (id, name, file, description, price, created_at, updated_at) VALUES (1, 'Scott Pilgrim''s Precious Little Life', '/images/product_file_1.jpg', 'Just when you thought you knew all there was to know about Scott Pilgrim comes Scott Pilgrim Color Hardcover Volume 1: Precious Little Life! The first in a series of brand-new hardcover editions, this remastered, 6"x9" hardcover presents Scott''s first "evil ex" battle as you''ve never seen it before - in full-color! Plus, previously unpublished extras and bonus materials make this mighty tome one that''s required reading for Scottaholics everywhere!', 79.99, '2018-05-27 04:05:42', '2018-05-27 04:05:42');
INSERT INTO store.products (id, name, file, description, price, created_at, updated_at) VALUES (2, 'The Girl From the Other Side: Siúil, A Rún Vol. 1', '/images/product_file_2.jpg', 'The Girl From the Other Side: Siúil, a Rún is an all-new manga series that pulls readers into a magical and enchanting tale about a young girl and her demonic guardian. A haunting story of love and fantastical creatures, The Girl From the Other Side: Siúil, a Rún is a gorgeously-illustrated manga series. Each volume in the series will be released with a beautifully-textured matte finish cover and will include at least one full-color insert.', 38.10, '2018-05-27 04:05:42', '2018-05-27 04:05:42');
INSERT INTO store.products (id, name, file, description, price, created_at, updated_at) VALUES (3, 'The Ancient Magus'' Bride Vol. 4', '/images/product_file_3.jpg', 'Chise has been summoned to the Dragon Aerie to begin crafting her very own wand, but her journey has more to offer than she had anticipated: magical wonders, enlightening visions, and perhaps most importantly, insight into Elias'' past and the secrets he has been reluctant to reveal about himself. Yet while Chise finds some answers about the inhuman mage''s history, mysterious beings are displaying an unexpected interest in Chise herself.', 37.34, '2018-05-27 04:05:42', '2018-05-27 04:05:42');
INSERT INTO store.products (id, name, file, description, price, created_at, updated_at) VALUES (4, 'Snotgirl Vol. 1: Green Hair Don''t Care', '/images/product_file_4.jpg', 'From bestselling BRYAN LEE O’MALLEY (Scott Pilgrim) and superstar newcomer LESLIE HUNG! Lottie Person is a glamorous fashion blogger living her best life—at least that’s what she wants you to think. The truth is, her friends are terrible people, her boyfriend traded her up for someone younger, her allergies are out of control, and she may or may not have killed somebody! SNOTGIRL VOL. 1 is the perfect introduction to one of 2016’s most buzzed-about titles! Collects SNOTGIRL #1-5.', 25.55, '2018-05-27 04:05:42', '2018-05-27 04:05:42');
INSERT INTO store.products (id, name, file, description, price, created_at, updated_at) VALUES (5, 'Wolverine: First Class #1', '/images/product_file_5.jpg', 'Professor X pairs up Kitty Pryde with Logan for a mision and neither of them are happy about it. But unless they learn to work together, neither of them will come back from their first mission together alive!', 39.99, '2018-05-27 04:05:43', '2018-05-27 04:05:43');
INSERT INTO store.products (id, name, file, description, price, created_at, updated_at) VALUES (6, 'Love and Freindship: And Other Youthful Writings', '/images/product_file_6.jpg', 'Jane Austen’s earliest writing dates from when she was just eleven-years-old, and already shows the hallmarks of her mature work. But it is also a product of the times in which she grew up—dark, grotesque, often surprisingly bawdy, and a far cry from the polished, sparkling novels of manners for which she became famous. Drunken heroines, babies who bite off their mothers’ fingers, and a letter-writer who has murdered her whole family all feature in these highly spirited pieces. This edition includes all of Austen’s juvenilia, including her “History of England” and the novella Lady Susan, in which the anti-heroine schemes and cheats her way through high society. With a title that captures a young Austen’s original idiosyncratic spelling habits and an introduction by Christine Alexander that shows how Austen was self-consciously fashioning herself as a writer from an early age, this is a must-have for any Austen lover.', 88.45, '2018-05-27 04:05:43', '2018-05-27 04:05:43');
INSERT INTO store.products (id, name, file, description, price, created_at, updated_at) VALUES (7, 'Marvel''s Jessica Jones #1 ', '/images/product_file_7.jpg', 'Meet Jessica Jones in this exclusive preview to the Netflix original series. All episodes streaming Nov 20.', 12.99, '2018-05-27 04:05:43', '2018-05-27 04:05:43');
INSERT INTO store.products (id, name, file, description, price, created_at, updated_at) VALUES (8, 'Black Panther: Soul Of A Machine (2017) #3', '/images/product_file_8.jpg', 'Chapter Three – Yet Philosophy: Wakanda has been infiltrated by the artificial intelligence known as Machinesmith. Can the Wakandan student scientists and their Takumi Master partners stop the spread of the Biotechnic Invasion?', 12.99, '2018-05-27 04:05:43', '2018-05-27 04:05:43');
INSERT INTO store.products (id, name, file, description, price, created_at, updated_at) VALUES (9, 'Guardiões do Louvre', '/images/product_file_9.jpeg', 'O aclamado mangaká Jiro Taniguchi o convida a conhecer o Museu do Louvre de uma maneira inesquecível. Depois de uma excursão pela Europa, um artista japonês faz uma parada em Paris sozinho, com a intenção de visitar os museus da cidade. Mas, acamado em seu hotel devido a febre, ele enfrenta o sofrimento da solidão absoluta em uma terra estrangeira, privado de qualquer recurso ou apoio familiar. Quando a febre baixa um pouco, ele inicia seus passeios e logo se perde nos monumentais salões do Louvre. Lá, descobre muitas facetas do mundo das artes, em uma jornada que oscila entre alucinações febris e realidade. Ele se vê conversando com pintores famosos de diversos períodos da história, sempre guiado pelos… Guardiões do Louvre.', 55.90, null, null);

INSERT INTO store.addresses (id, user_id, address, complement, city, state, country, post_code, name, phone, created_at, updated_at) VALUES (1, 2, 'Luiz Soares de Araujo, 1-56, Mary Dota', null, 'Bauru', 'Sao Paulo', 'Brasil', '17026-450', 'Leonardo Henrique Benedette Lopes', null, null, null);
INSERT INTO store.addresses (id, user_id, address, complement, city, state, country, post_code, name, phone, created_at, updated_at) VALUES (2, 2, 'R. Vítor Manoel Souza Lima, 410 - Jardim Bethania,', 'Apartamento 32', 'Bauru', 'São Paulo', 'Brazil', '13561-020', 'Leonardo Henrique Benedette Lopes', '14991026669', '2018-05-27 21:27:55', '2018-05-27 21:27:55');

INSERT INTO store.orders (id, user_id, address, value, created_at, updated_at) VALUES (2, 2, '{"id": 1, "city": "Bauru", "name": "Leonardo Henrique Benedette Lopes", "phone": null, "state": "Sao Paulo", "address": "Luiz Soares de Araujo, 1-56, Mary Dota", "country": "Brasil", "user_id": 2, "post_code": "17026-450", "complement": null, "created_at": null, "updated_at": null}', 50.33, '2018-05-27 17:40:54', '2018-05-27 17:40:54');
INSERT INTO store.orders (id, user_id, address, value, created_at, updated_at) VALUES (3, 2, '{"id": 1, "city": "Bauru", "name": "Leonardo Henrique Benedette Lopes", "phone": null, "state": "Sao Paulo", "address": "Luiz Soares de Araujo, 1-56, Mary Dota", "country": "Brasil", "user_id": 2, "post_code": "17026-450", "complement": null, "created_at": null, "updated_at": null}', 118.09, '2018-05-27 17:52:34', '2018-05-27 17:52:35');
INSERT INTO store.orders (id, user_id, address, value, created_at, updated_at) VALUES (9, 2, '{"id": 2, "city": "Bauru", "name": "Leonardo Henrique Benedette Lopes", "phone": "14991026669", "state": "São Paulo", "address": "R. Vítor Manoel Souza Lima, 410 - Jardim Bethania,", "country": "Brazil", "user_id": 2, "post_code": "13561-020", "complement": "Apartamento 32", "created_at": "2018-05-27 21:27:55", "updated_at": "2018-05-27 21:27:55"}', 37.34, '2018-05-27 21:28:39', '2018-05-27 21:28:40');

INSERT INTO store.order_product (order_id, product_id) VALUES (3, 1);
INSERT INTO store.order_product (order_id, product_id) VALUES (3, 2);
INSERT INTO store.order_product (order_id, product_id) VALUES (2, 3);
INSERT INTO store.order_product (order_id, product_id) VALUES (9, 3);
INSERT INTO store.order_product (order_id, product_id) VALUES (2, 8);

INSERT INTO store.categories (id, category, created_at, updated_at) VALUES (1, 'Hq', null, null);
INSERT INTO store.categories (id, category, created_at, updated_at) VALUES (2, 'Mangá', null, null);
INSERT INTO store.categories (id, category, created_at, updated_at) VALUES (3, 'Livro', null, null);
INSERT INTO store.categories (id, category, created_at, updated_at) VALUES (4, 'Clássicos', null, null);
INSERT INTO store.categories (id, category, created_at, updated_at) VALUES (5, 'Aventura', null, null);
INSERT INTO store.categories (id, category, created_at, updated_at) VALUES (6, 'Romance', null, null);
INSERT INTO store.categories (id, category, created_at, updated_at) VALUES (7, 'Drama', null, null);
INSERT INTO store.categories (id, category, created_at, updated_at) VALUES (8, 'Inglês', null, null);
INSERT INTO store.categories (id, category, created_at, updated_at) VALUES (9, 'Preview', null, null);

INSERT INTO store.category_product (category_id, product_id) VALUES (1, 1);
INSERT INTO store.category_product (category_id, product_id) VALUES (5, 1);
INSERT INTO store.category_product (category_id, product_id) VALUES (8, 1);
INSERT INTO store.category_product (category_id, product_id) VALUES (2, 2);
INSERT INTO store.category_product (category_id, product_id) VALUES (6, 2);
INSERT INTO store.category_product (category_id, product_id) VALUES (7, 2);
INSERT INTO store.category_product (category_id, product_id) VALUES (2, 3);
INSERT INTO store.category_product (category_id, product_id) VALUES (6, 3);
INSERT INTO store.category_product (category_id, product_id) VALUES (7, 3);
INSERT INTO store.category_product (category_id, product_id) VALUES (1, 4);
INSERT INTO store.category_product (category_id, product_id) VALUES (7, 4);
INSERT INTO store.category_product (category_id, product_id) VALUES (1, 5);
INSERT INTO store.category_product (category_id, product_id) VALUES (5, 5);
INSERT INTO store.category_product (category_id, product_id) VALUES (8, 5);
INSERT INTO store.category_product (category_id, product_id) VALUES (3, 6);
INSERT INTO store.category_product (category_id, product_id) VALUES (4, 6);
INSERT INTO store.category_product (category_id, product_id) VALUES (6, 6);
INSERT INTO store.category_product (category_id, product_id) VALUES (8, 6);
INSERT INTO store.category_product (category_id, product_id) VALUES (1, 7);
INSERT INTO store.category_product (category_id, product_id) VALUES (5, 7);
INSERT INTO store.category_product (category_id, product_id) VALUES (7, 7);
INSERT INTO store.category_product (category_id, product_id) VALUES (9, 7);
INSERT INTO store.category_product (category_id, product_id) VALUES (1, 8);
INSERT INTO store.category_product (category_id, product_id) VALUES (5, 8);
INSERT INTO store.category_product (category_id, product_id) VALUES (9, 8);
INSERT INTO store.category_product (category_id, product_id) VALUES (2, 9);
INSERT INTO store.category_product (category_id, product_id) VALUES (4, 9);

INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (1, 1, 'Capa dura', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (2, 2, 'Capa comum', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (3, 2, '180 páginas ', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (4, 3, 'Capa Comum', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (5, 4, 'Capa Comum', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (6, 4, '136 páginas ', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (7, 5, 'eBook', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (8, 5, '25 páginas ', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (9, 5, 'Idioma: Inglês', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (10, 6, 'Capa Dura', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (11, 6, 'Idioma: Inglês', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (12, 7, 'eBook', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (13, 7, '11 páginas ', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (14, 8, 'eBook', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (15, 8, '11 páginas ', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (17, 9, 'Capa dura', null, null);
INSERT INTO store.characteristics (id, product_id, characteristic, created_at, updated_at) VALUES (18, 9, 'Dimensoes 31,6 x 23,4 x 1,8 cm', null, null);