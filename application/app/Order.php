<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $fillable = ['user_id', 'address'];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function address()
    {
        return json_decode($this->address);
    }

    public static function create($user_id, Address $address)
    {
        $order = new Order();

        try {
            DB::beginTransaction();

            $order->user_id = $user_id;
            $order->address = $address->toJson();
            $order->save();
            $order->setProducts(Cart::getCartProducts($user_id));
            $order->save();

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
            return false;
        }

        return $order;
    }

    public function setProducts($products)
    {
        $value = 0.00;
        foreach ($products as $product) {
            $this->products()->save($product);
            // somar o valor dos produtos
            $value += $product->price;
        }
        $this->value = $value;
        $this->save();
    }
}
