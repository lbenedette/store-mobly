<?php

function money($value)
{
    $priceString = number_format($value, 2, ',', '');
    return '<span class="price">' . 'R$ ' . $priceString . '</span>';
}