<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function Orders()
    {
        // no momento estou apenas pegando pelo mais novo
        // caso fosse necessario buscar orders de outras formas
        // seria interessante implementar uma funçao com scope
        return $this->hasMany(Order::class)->latest();
    }
}
