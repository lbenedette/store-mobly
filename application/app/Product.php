<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function path()
    {
        return '/products/' . $this->id;
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category')->pluck('category');
    }

    public function characteristics()
    {
        return $this->hasMany('App\Characteristic')->pluck('characteristic');
    }
}
