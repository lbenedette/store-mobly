<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Redis;

// No momento a classe esta bem simples, mas atende todas as necessidades.
// Caso fosse preciso, poderiamos melhorar esta classe.
// talvez associar com User e acessar as funçoes atraves dele
class Cart
{
    protected static $REDIS_KEY = 'cart:products:';

    public static function getCart($key)
    {
        $cart = Redis::get($key);

        return $cart ? json_decode($cart) : [];
    }

    public static function getCartProducts($user_id)
    {
        $key = self::$REDIS_KEY . $user_id;

        $cart = self::getCart($key);

        $products = new Collection();
        foreach ($cart as $product) {
            $products->add(Product::find($product));
        }

        return $products->count() ? $products : false;
    }

    public static function addProduct($user_id, Product $product)
    {
        $key = self::$REDIS_KEY . $user_id;

        $cart = self::getCart($key);

        $cart[] = $product->id;

        Redis::set($key, json_encode($cart));
    }

    public static function removeProduct($user_id, $position)
    {
        $key = self::$REDIS_KEY . $user_id;

        $cart = self::getCart($key);

        unset($cart[$position]);

        // array_values, pois depois do unset, o php adiciona as keys
        Redis::set($key, json_encode(array_values($cart)));
    }

    public static function getValue($user_id)
    {
        $products = self::getCartProducts($user_id);

        return $products ? array_sum($products->pluck('price')->toArray()) : 0;
    }

    public static function clear($user_id)
    {
        $key = self::$REDIS_KEY . $user_id;

        Redis::del($key);
    }
}