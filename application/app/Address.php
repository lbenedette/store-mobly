<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'user_id', 'address', 'complement', 'city', 'state', 'country',
        'post_code', 'name', 'phone'
    ];
}
