<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('buy');
    }

    public function index()
    {
        $search = \request('search');

        if ($search) {
            $products = Product::where('name', 'like', '%' . $search . '%')->get();
        } else {
            $products = Product::all();
        }

        return view('products.index', compact('products'));
    }

    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    public function buy(Product $product)
    {
        Cart::addProduct(Auth()->id(), $product);

        return back()
            ->with('flash_message', 'Produto adicionado ao carrinho com sucesso!')
            ->with('flash_class', 'success');
    }
}
