<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $addresses = Auth()->user()->addresses;

        return view('addresses.index', compact('addresses'));
    }

    public function create()
    {
        return view('addresses.create');
    }

    public function store(Request $request)
    {
        $address = new Address([
            'user_id' => Auth()->id(),
            'name' => $request->name,
            'address' => $request->address,
            'complement' => $request->complement,
            'city' => $request->city,
            'state' => $request->state,
            'country' => $request->country,
            'post_code' => $request->post_code,
            'phone' => $request->phone,
        ]);

        $address->save();

        return redirect('/addresses')
            ->with('flash_message', 'Endereço criado com sucesso!')
            ->with('flash_class', 'success');
    }
}
