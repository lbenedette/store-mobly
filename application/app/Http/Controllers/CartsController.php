<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use Illuminate\Http\Request;

class CartsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth()->user();

        $products = Cart::getCartProducts(Auth()->id());
        if (!$products) {
            return redirect('/products')
                ->with('flash_message', 'Você não possui produtos no carrinho!')
                ->with('flash_class', 'warning');
        }

        $addresses = $user->addresses;

        return view('carts.index', compact('products', 'addresses'));
    }

    public function removeProduct(Request $request)
    {
        $product = Product::find($request->product_id);
        if (!$product) {
            return json_encode([
                'result' => false,
                'message' => 'Produto não encontrado!'
            ]);
        }

        $user_id = Auth()->id();

        Cart::removeProduct($user_id, $request->position);

        return json_encode([
            'result' => true,
            'message' => 'Produto removido com sucesso!',
            'value' => money(Cart::getValue($user_id))
        ]);
    }

    public function destroy()
    {
        Cart::clear(Auth()->id());

        return redirect('/products')
            ->with('flash_message', 'Produtos removidos do carrinho com sucesso!')
            ->with('flash_class', 'success');
    }
}
