<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
}
