<?php

namespace App\Http\Controllers;

use App\Address;
use App\Cart;
use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $orders = Auth()->user()->orders;
        // Seria legal implementar uma paginaçao conforme o numero de pedidos fosse aumentando
        return view('orders.index', compact('orders'));
    }

    public function store(Request $request)
    {
        $user = Auth()->user();
        $address = Address::find($request->address);

        // construir um metodo para refatorar essas verificacoes
        if (!$address) {
            return redirect('/carts')
                ->with('flash_message', 'Endereço não encontrado!')
                ->with('flash_class', 'danger');
        }

        $order = Order::create($user->id, $address);
        if (!$order) {
            return redirect('/carts')
                ->with('flash_message', 'Ocorreu um erro ao salvar o pedido!')
                ->with('flash_class', 'danger');
        }

        Cart::clear($user->id);
        return redirect('/orders')
            ->with('flash_message', 'Pedido realizado com sucesso!')
            ->with('flash_class', 'success');
    }
}
