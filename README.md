# store-mobly
Tutorial para deploy do projeto:

O simbolo $ indica comandos do terminal

Executando o docker-compose:
==
(É necessário ter o docker e o docker-compose em sua máquina)

a) Após clonar o projeto, entrar na pasta do projeto.

b) $ cd docker/

c) $ docker-compose up --build 
(Isto pode demorar um tempo!)

Compose e Laravel
==

a) $ docker exec -ti php-store bash
(Entrar no docker do php)

b) $ cd application/

c) $ composer install
(Isto também pode demorar um tempo!)

d) $ cp .env.example .env
(As configurações do .env.example já estão corretas)

e) $ php artisan key:generate

f) $ php artisan migrate

g) $ chmod 777 -R storage/

Banco de dados
==

a) $ cd script/

b) $ mysql -hdb -p store < store.sql 

c) Quando for pedido a senha digite: q1w2e3

Informações
==
Existem 2 usuários criados:
email: john@example.com senha: secret
email: blopesleo@outlook.com senha: ugauga01

É possível criar novos usuários, esqueci a senha não funciona.
 
